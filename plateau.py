from typing import Tuple, List
from rover import Rover


class Plateau:
    def __init__(self, max_x, max_y):
        self.max_x = max_x
        self.max_y = max_y

    def place_rover_and_execute_commands(self, rover: Rover, commands: List[str]) -> Tuple[int, int, str]:
        *_, last = map(lambda command: rover.execute(command, self.max_x, self.max_y), commands)
        return last
