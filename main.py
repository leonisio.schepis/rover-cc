from typing import List

from plateau import Plateau
from rover import Rover


def run(input_text) -> str:
    plateau = build_plateau(input_text[0].strip())
    rover_command_pair = map(lambda x: (build_rover(x[0].strip()), build_commands(x[1].strip())), split_rover_and_commands(input_text))
    results = map(lambda x: plateau.place_rover_and_execute_commands(x[0], x[1]), rover_command_pair)
    results_string = map(lambda x: f"{x[0]} {x[1]} {x[2]}", results)
    return "\n".join(results_string)


def split_rover_and_commands(input_list: List[str]) -> str:
    for i in range(1, len(input_list), 2):
        yield input_list[i:i+2]


def build_plateau(plateau_string) -> Plateau:
    max_x, max_y = plateau_string.split(' ')
    if max_x.isdigit() and max_y.isdigit():
        return Plateau(int(max_x), int(max_y))
    else:
        raise RuntimeError("malformed input, ensure that firs line contains well-formed plateau size like '5 5'")


def build_rover(rover_string) -> Rover:
    rover_input_list = rover_string.split(' ')
    if rover_input_list[0].isdigit() and rover_input_list[1].isdigit() and rover_input_list[2] in ['N', 'W', 'S', 'E']:
        return Rover(int(rover_input_list[0]), int(rover_input_list[1]), rover_input_list[2])
    else:
        raise RuntimeError("malformed input, ensure that odd lines contain well-formed coordinate like '0 0 N'")


def build_commands(commands_string) -> List[str]:
    commands_string_split = commands_string.split(' ')
    if all(command in ['L', 'R', 'M'] for command in commands_string_split):
        return commands_string_split
    else:
        raise RuntimeError("malformed input, ensure that even lines (except first one) contain only L, R or M")


if __name__ == '__main__':
    with open("input.txt") as input_file:
        print(run(input_file.readlines()))
