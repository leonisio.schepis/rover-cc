from typing import Tuple

from compass import Compass


class Rover:
    def __init__(self, initial_x: int, initial_y: int, initial_direction: str):
        self.x = initial_x
        self.y = initial_y
        self.compass = Compass(initial_direction)

    def execute(self, command: str, max_x: int, max_y: int) -> Tuple[int, int, str]:
        """
        :param command: the command to execute
        :param max_x: the plateau max x
        :param max_y: the plateau max y
        :rtype: A triplet containing the updated position after the command
        """
        if command != 'M':
            self.compass.turn_to(command)
        else:
            if self.compass.direction == 'W':
                self.x = max(0, self.x - 1)
            elif self.compass.direction == 'E':
                self.x = min(max_x, self.x + 1)
            elif self.compass.direction == 'S':
                self.y = max(0, self.y - 1)
            else:
                self.y = min(max_y, self.y + 1)
        return self.x, self.y, self.compass.direction
