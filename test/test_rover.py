import unittest
from main import run


class TestRover(unittest.TestCase):

    def test_execute_provided_example(self):
        test_input = """5 5
                        1 2 N
                        L M L M L M L M M
                        3 3 E
                        M M R M M R M R R M"""
        expected_output = "1 3 N\n5 1 E"
        self.assertEqual(run(test_input.split('\n')), expected_output)

    def test_execute_in_corners(self):
        test_input = """5 5
                        0 0 W
                        M L M
                        5 5 E
                        M L M"""
        expected_output = "0 0 S\n5 5 N"
        self.assertEqual(run(test_input.split('\n')), expected_output)

    def test_givenMalformedInput_whenRun_thenRuntimeError(self):
        test_input = """5 K
                        1 2 N
                        L M L M L M L M M"""
        self.assertRaises(RuntimeError, lambda: run(test_input.split('\n')))

    def test_givenMalformedInput2_whenRun_thenRuntimeError(self):
        test_input = """5 5
                        1 K N
                        L M L M L M L M M"""
        self.assertRaises(RuntimeError, lambda: run(test_input.split('\n')))

    def test_givenMalformedInput3_whenRun_thenRuntimeError(self):
        test_input = """5 5
                        1 2 K
                        L M L M L M L M M"""
        self.assertRaises(RuntimeError, lambda: run(test_input.split('\n')))

    def test_givenMalformedInput4_whenRun_thenRuntimeError(self):
        test_input = """5 5
                        1 2 N
                        L M L K L M L M M"""
        self.assertRaises(RuntimeError, lambda: run(test_input.split('\n')))


if __name__ == '__main__':
    unittest.main()
