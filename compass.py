class Compass:
    def __init__(self, initial_direction: str):
        self.cardinal_encoder = dict()
        self.cardinal_encoder['N'] = 0
        self.cardinal_encoder['E'] = 1
        self.cardinal_encoder['S'] = 2
        self.cardinal_encoder['W'] = 3
        self.cardinal_decoder = {v: k for k, v in self.cardinal_encoder.items()}
        self.direction = initial_direction

    def turn_to(self, left_or_right: str) -> str:
        """
        :param left_or_right: "L" or "R", indicating a 90 degrees rotation in a direction
        :rtype: The new cardinal direction ("N", "S", "W", "E") faced
        """
        cardinal = self.cardinal_encoder[self.direction]
        cardinal = (cardinal - 1 if left_or_right == 'L' else cardinal + 1) % 4
        self.direction = self.cardinal_decoder[cardinal]
        return self.direction
